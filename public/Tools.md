# Tools

> Point B default tools are in *italics*

## Notes
- [*OneNote*](https://www.office.com/launch/onenote)
- [Sticky Notes](https://www.microsoft.com/en-us/p/microsoft-sticky-notes/9nblggh4qghw) 
- [Hugo](https://www.hugo.team/)

## Whiteboards
- [*Mural*](https://mural.co/)
- [LucidChart](https://www.lucidchart.com/documents)
- [Microsoft Whiteboard](https://www.microsoft.com/en-us/p/microsoft-whiteboard/9mspc6mp8fm4)

## Chat
- [*Teams*](https://aka.ms/mstfw)
- [Chatter](https://pointb.lightning.force.com/one/one.app)
- Skype for Business
- [Yammer](https://www.yammer.com/pointb.com/)

## Process Flows/Diagrams
- [*Lucid Chart*](https://www.lucidchart.com/documents)
- Visio

## Project Management
- [*Jira*](https://pointb.atlassian.net)
- MS Project

## Incident Management
- [*ServiceNow*]()

## Task Management
- [*Jira*](https://pointb.atlassian.net)
- Outlook Tasks
- [MS Planner](https://tasks.office.com/)

## Surveys and Quizzes
- [*Forms*](https://forms.office.com/)

## Code Repository
- [*Bitbucket*](https://bitbucket.org/)

## Reporting and Data Analytics
- Tableau
- Power BI