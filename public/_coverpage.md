# BUSINESS ANALYSIS TOOLKIT

![logo](_media/toolbox.png)

> Documentation guidelines, tips and templates for Biz Apps BAs and PMs

<div class="buttons">
<a href="#/README.md"><span>Get Started</span></a>
<a href="https://gitlab.com/biz-apps/ba-toolkit" target="_blank"><span>Edit or Add Docs</span></a>
</div>

<!-- background color -->
![color](#1C2033)