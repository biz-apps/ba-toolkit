![BA Toolkit](_media/toolkit_logolarge.png)

 >**What is this?**\
>The Point B Biz Apps Toolkit aims to document our approach to business analysis and encourage consistency across all internal project delivetry. The toolkit covers agile requirments gathering including style, storage, sharing guidelines, and templates. These guidelines are open, collaborative, and always changing based on feedback. If you have improvements or updates to this documentation, edits can be made via bitbucket. 