# Requirements Guidelines



[//]: # (## Requirments Gathering Lifecycle
The requirements gathering lifecycle is a general outline that is not meant to be perscriptive. However, it can be used as guideposts on complex projects or to describe our approach to our customers.)

[//]: # (The Lifecycle is as follows:)
[//]: # (* Identify the high level scope)
[//]: # (* Identify an architectual vision)
[//]: # (* Elicit and document requirements)
[//]: # (* Manage, update, and track requirements)


## Documentaton, Storage, and Collaboration
### Jira
Requirements can be captured using [Jira](http://www.pointb.atlassian.net), our issue management tool. There is a special issue type abvailable on most projects called 'Requirement'. The requirement issue type has a unique workflow and special fields in JIRA specially made for tracking requirements. 

>If the Requirement issue type is not available for a project, contact a Jira admin to have it added.  

![Creating a new requirement in JIRA](_media/jira_requirement.png)

Requirements should be tied to epics in Jira and then linked to resulting tasks and subtaks for traceability using standard Jira functionality. As requirements evolve, Jira issues can be updated using the editing or commenting functionality in Jira. Jira also has a LucidChart plugin that allows the attachment of live process flows and other diagrams. 

![Adding a LucidChart Diagram in Jira](_media\jira_lucidchart.png)

Requirements can be recorded in an exsiting Jira project or you can create a new next-gen project if you need more control over requirements workflow.

### Spreadsheets & Tables
Requirements can be exported from Jira as a spreadsheet, but if preferable, requirements can also be captured using simple spreadsheet formats or in tables. In order to make the requirements durable and collaborative, the documents should be shared via Teams as early as possible and edited in place. Comments can be tracked directly in Teams as well. If a Team doesn't exsist as a place to store your requirements doc, you can use the [**Point B IT > Business Applications Team**](https://teams.microsoft.com/l/channel/19%3a361383bf13a5441685da3a565424858a%40thread.skype/Business%2520Applications?groupId=84bd1e8f-da35-4570-8d28-04b5050337cd&tenantId=2e0d020c-5368-4963-a295-44297d7fd521) for storage.


## Requirements Format
### User Story
User stories are user-centric statements written in plain language that provide a simple framwork for capturing requirements. 

While we use the user story format to track simple requirements, more complex requirements will require more details and stories should point to diagrams or other artifacts.

#### Format
User stories at Point B follow the standard format: **As a *< person or persona >*, I want *< some goal >* so that *< some reason >*.**

#### Acceptance Criteria 
We can add detail and track the completion of a requirement right in a user story using acceptance criteria. Adding acceptance criteria to a user story should be as simple as adding bulleted list.

#### Example
* As a user, I am required to enter a strong password when creating my account.
  * Acceptance criteria:
    * Must have at least 8 characters
    * Must contain at least 1 digit
    * Must contain at least 1 uppercase letter
    * Must contain at least 1 lowercase letter
    * Must contain at least 1 symbol


### Generic Requirements
When not written in the user story format, requirments should be clear and unambiguaous.

#### Format
**Ability to *< do something >* using *< something >*.**

#### Example
Ability to search for a customer record using the following search criteria:
* Name
* Location
* Organization
* ID

## Requiements Gathering Techniques 

### Requiments Modeling
**Best practices:** Models are visual representations of information related to processes, data, and interactions. We use models to organize and represent complex requirements. Our goal is to collaboratively build requirements models with stakeholders to elicit and validate requirements. Some examples of types of requirements models:
* Process Flows
* Org Charts
* RACIs
* Data Flow Diagrams
* Domain Models
* Decision Trees

>Tip: Check the [templates section](Templates.md) of this guide to find templates for some of the ses models. Don't forget to attach your models to your Jira requirements, too.

### Prototyping
**Best practices:** Use a prototype when working with complex systems, new workflows, or anytime something really needs to be seen to be understood. Capture comments directly on the prototype to track feedback in context. Keep the prototype as low fidelity as you can so incorporating changes is quick and not cumbersome. 
>Tip: Use the exsisting system/sandbox  to build a quick prototype or use tools like PowerPoint, LucidChart, or Mural ([Tools](Tools.md)) to build protoypes with drawings or screenshots.

### Interviews
**Best practices:** Prepare an agenda and share it with the subject(s) before the interview. Take notes and share them along with any follow-up questions and next steps. 
>Tip: Use a digital whiteboard ([Tools](Tools.md)) during the interview to take visual notes

### Observation
**Best practices:** Observation is a difficult approach in a distributed workforce, so use screen recording tools (e.g. Skype) to watch and review how customers accomplish goals. As much as possible, save questions until after the obsservation session to avoid influencing how the customer acts. Take notes and share them along with any follow-up questions and next steps. 

### Surveys
**Best practices:** Use surveys when you have a large stakeholder group and need to capture a wide range of opinions. Follow these [guidelines](https://www.surveymonkey.com/mp/survey-guidelines/) to create an effective survey.
>Tip: You can create and share surveys using Office 365 forms ([Tools](Tools.md)). Tracking responses is simple using the built in reporting tools.

![SurveyResponses](_media/SurveyResponses.png) 