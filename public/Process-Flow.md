# Process Flow Guidelines

Your stakeholders and customers have likely seen a lot of process flows. So many, in fact, that they are starting to blur together. So what can you do to make your process flow stand out and improve understanding? Use these tips as a guidelines to building great flows.

## Example
![](_media/ProcessFlow(Colorful).png)

## Templates
[Generic Flow B&W](https://www.lucidchart.com/invitations/accept/19723ec0-938d-4a85-b5c9-708ee76649d0)\
[Generic Flow Colorful](https://www.lucidchart.com/invitations/accept/357d8a16-07ee-4058-8d57-fd02ee5fe2e3)

# Guidelines
## Swimlanes
>Use **Swimlanes** To Differentiate Between Task Owners

![Swimlanes](_media/swimlanes.png)

Swimlanes are horizontal or vertical visual divisions used to illustrate responsibilities and ownership. Swimlanes make your flow easier to read and grasp, especially at first glance. Use swimlanes to establish the actors in a process and to identify responsibility for each step. 

Getting swimlanes right is important for the lasting value of a process flow. After the initial creation and distribution of the document, illustrating responsibility for tasks can be the most valuable use case for the document. 

Even process flows with only one actor should use a swimlane. This makes it clear to your audience that each task still has ownership, albeit a solitary owner.

## Process Boxes
> **Never** Put A Process Box In The Middle Of Two Swimlanes.
In a process flow, only connectors should cross swimlanes not tasks or decisions.


It can be tempting to stick a process box or decision point between two swimlanes - visually straddling the line between ownership. Sometimes it does feel like a task is owned by multiple entities. But, on top of being confusing and general bad practice, it also devalues the process flow by failing to establish clear ownership. 

Process boxes that straddle lines can look like a mistake. And if you add too many of them in a single flow it becomes distracting. When you are tempted to include a process, task, or decision that straddles a line try breaking it down to smaller parts instead. 

Keep breaking down the process until you have a clear division of ownership and can place boxes into a single swimlane.

 
## Clarity
>Make Sure The Path From **Start To End** Is Clear. Label lines when absolutely necessary.

![](_media/ClearPath.png)

The dual goals of a process flow are to visualize a complex process and simplify the process for understanding. 

If a flow is full of branching decisions and off-page references, consider small variations in line weight or saturation to call out the critical path. Can't decide on the best path to highlight? Try focusing on the happy path (i.e. a path with no exceptions) or the path that has the highest frequency. 

At every decision point, ask your stakeholder which outcome happens most often and follow that path all the way to the end.

 
## Connectors
> Use **right angle** elbows on your lines; it is easier for the eye to follow


LucidChart and Visio both give users a choice on how connectors are displayed in a flow, and the options are usually right angles, curves, and straight lines. Each style has its appeal, and straying from the norm can give your diagram some visual flair. 

But, with most process flows, it is best to stick to the right angle connectors. Right angle connectors are easiest for the eye to follow whenever you have tasks spread across multiple swimlanes, and if you have a lot of decision points and branching connectors curved lines can get unwieldy and confusing.

You can also try using each style of connector to see what works best for your document. Take a few steps back from the screen and see if you can still easily follow the flow from a distance. If the directional thrust of the flow starts to get lost when viewing the flow from afar, try another style to see if it improves the clarity.
 
## Shapes
> Use a shape library your stakeholders will **understand**, like BPMN or UML.

![](_media/BPMN.png)

Build your flows using standard shapes whenever possible. Even if you don't follow standards exactly (like all the routing rules, for example) using the shapes and their standard definitions will make your documents sturdier, clearer, and reusable. 

Also, try to avoid including a legend of any type on your process flows. Either clarify the flow using specific shapes, use in place labels, simplify the flow, or break up the legend and incorporate the labels in context.

Find out more about BPML from the [Object Management Group](http://www.bpmn.org).

## Backgrounds
> Avoid unnecessary **gradients** and distracting **backgrounds**. It may look 'pretty' or 'cool', but if a background or a gradient dosen't enhance your stakeholders understanding of a process, leave it out.

LucidChart and Visio come overflowing with options for styling your process flows. You can add custom page backgrounds, various typefaces, shadows, colors, gradients, and 3D effects to your flow. And while it may be tempting to explore all the options at your fingertips, you should practice restraint when adding all that style to the page. Remember to only add style when it aids in building understanding of the process.

A lot of software also comes pre-loaded with themes and color schemes. Themes allow you to change your document design and styles on the fly. Be wary of these out of the box themes. These themes are not designed with your document in mind. A theme may look good in a thumbnail in the style dropdown but it might not make sense for your document. 

While it is simple to switch up your document's style, a lot of the themes use flashy effects like gradients, backgrounds, and shadows. Looking at a document with all that style can cause fatigue in your stakeholders eyes and brains
 
## Starts and Ends
>Every process flow has at least one **start** and one **end.**



Try not to leave the beginning and ending of your process flow ambiguous. A clearly labeled beginning and end will help your stakeholders get their bearings and understand the scope of your flow. 

Putting your start on the far left and your end on the far right of the page helps users quickly visualize the relative complexity of the flow. Resist sticking any tasks to the left of the first task or to the right of the last task.

## Color
> Use color sparingly. Make sure that color is **meaningful** and is drawing your stakeholders eye to the most important tasks or steps. The less color you use, the more impact each color will have in your flow. 

![](_media/ColorPop.png)

When it comes to a process flow and color, less is more. Try to avoid making every shape on your page the default blue. Or, even worse, making every shape on your page a different color. 

For a process flow, approach color formatting the same way you would approach formatting text in an email. You (hopefully) wouldn't make all text in an email all caps, or all bold, or all italic. You might apply that formatting at certain points to call out important details or to clarify your message. 

The same is true with a process flow. Only use color when it really aids in stakeholder understanding. And don't spend all day trying to pick the perfect colors.

Also, if your flow is going to be printed, it should be designed to work in grayscale. Limiting your colors will help make printing easier to manage. 

## Flow
>Try to flow the process from **left** to **right**, and **top** to **bottom**. Loop back if needed, but keep the process flow's general direction going left to right.


Our processes are elaborate, complex and confusing. If they weren't, we wouldn't take the time and effort to document them in process flows. But, sometimes a process flow can get so detailed and byzantine that is is tough to make heads or tails of the directional flow.

Follow the best practice of starting the flow in the top left corner and working you way to the right (and bottom). For example, don't have to place executive roles at the top of the spreadsheet just because it is at the top of the org chart. 

Put the swimlanes in the order that makes sense for the process. It is less confusing for your stakeholders and they will appreciate the consistency. And reviewing the flows later much easier as well. 

## Title
>**Title** your flow. Adding a title to your process flow is one of the easiest ways to add clarity to your document.

Add a title to your process flow and make it visible on the document. This one is easy to do and easy to forget. A title can add so much clarity for your stakeholders, especially when they are looking at a lot of different process flows.

Try to us an action verb in the title that matches the goals of the process. For example a title of Recognize Revenue is preferable to the generic Revenue Recognition Process. You might also want to include the primary owner in the title, be it an organization or an individual. 

For flows that are meant to be used and referenced for more than a few days, stick with a organization or role as individuals may come and go. Remember, if you have strict file naming conventions the title doesn't have to match the filename. Aim for clarity and simplicity for the title.

Place the title where your stakeholders are most likely to look for it - in the top left corner of your document.

## Alignment
>Make sure your shapes are aligned and spaced for maximum **legibility**. 

LucidChart and Visio will automatically align and distribute your shapes as you build the flow. Use the software to your advantage and align your shapes within their swimlanes and with each other. 

Keep your connections straight and allow enough space for labels to be legible and connections clear. Double check to make sure your arrows are pointing the right direction. Remember to embrace space! 

In comparison with an aligned and well-spaced flow, a cluttered document is less legible and actually looks like you spent less time working on it.
