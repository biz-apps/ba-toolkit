# Domain Model Guidelines
*If you only model one thing in, model the domain.*

## Example
![Domain Model Example](_media/DomainModelColorProblemDomain.png)

## Description
From [Scaled Agile Framework](https://www.scaledagileframework.com/domain-modeling/):
>Domain Modeling is a way to describe and model real world entities and the relationships between them, which collectively describe the problem domain space. Derived from an understanding of system-level requirements, identifying domain entities and their relationships provides an effective basis for understanding and helps practitioners design systems for maintainability, testability, and incremental development. Because there is often a gap between understanding the problem domain and the interpretation of requirements, domain modeling is a primary modeling area in Agile development at scale. Driven in part from object-oriented design approaches, domain modeling envisions the solution as a set of domain objects that collaborate to fulfill system-level scenarios.


## Usage
A domain model can represent a collection of systems and processes or processes inside of a single system, but is should represent the entire domain being addressed by the problem.

Use a domain model to:
* Communicate complex interactions to non-technical stakeholders
* As part of a requrements session or design workshop
* Aid in system design
* Map impacts of changes, epics, or requirements (see example)

![Domain Model with Mapping Overlay](_media/DomainModelMapped.png)